TiaAmparo::Application.routes.draw do

  get "tinymce_assets/create"

################disenyo_restauracion###############################
  get "disenyo/create"
  match 'disenyo' => 'disenyo#new', :as => 'disenyo', :via => :get
  match 'disenyo' => 'disenyo#create', :as => 'disenyo', :via => :post
################disenyo_restauracion#################

############zona verde######################
  resources :comments
  get "blog/index"
  match 'blog' => 'blog#new', :as => 'blog', :via => :get
  match 'blog' => 'blog#create', :as => 'blog', :via => :post
  post '/tinymce_assets' => 'tinymce_assets#create'
  resources :articles do
    collection do
      put :busqueda
    end
    resources :comments
  end
############zona verde###########################

############preguntas_frecuentes###########################
  get "preguntas_frecuentes/index"  ,as: 'preguntas_frecuentes'
############preguntas_frecuentes###########################

#########admin#########################
  get 'admin' => 'admin#index'
  get 'limpiezaBD' => 'admin#limpiezaBD'
#########admin##########################

#########usuarios#########################
  resources :password_resets
  controller :sessions do
    get 'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

  resources  :users do
    collection do
      get :areaUsuario
      put :busqueda
    end
  end
##########usuarios########################

##########tienda########################
  resources :gastos_envios do
    collection do
      get :resetearValores
      get :gastosEnvioCero
    end
  end
  resources  :line_items,:carts
  match 'store' => 'store#index', :as => 'store', :via => :get
  get "/store/index"
  get "/store/ceramica"
  get "/store/pintura"
  get "/store/jabones_naturales"
  get "/store/serijos"
  get "/store/bisuteria"
  get "/store/decoracion"
  get "/store/antiguedades"
  get "/store/muebles"
  get "/store/vinos"
  get "/store/quesos"
  get "/store/souvenires"
  get "/store/varios"
  put "/store/busqueda"

  resources :products do
    collection do
      put :busqueda
    end
  end

  resources :orders do
    collection do
      put :busqueda
    end
  end
##########tienda########################

############galeria_fotos######################
  resources :galeria_fotos do
    collection do
      get :verGaleriaFotos
      put :busqueda
    end
  end
############galeria_fotos#####################

############contacto#####################
  get "contacto/create"
  match 'contacto' => 'contacto#new', :as => 'contacto', :via => :get
  match 'contacto' => 'contacto#create', :as => 'contacto', :via => :post
############contacto#####################

############inicio#####################
  get "inicio/index"
  root to: 'inicio#index' , as: 'inicio'
############inicio#####################
end
