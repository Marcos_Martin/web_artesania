root = "/home/web/apps/web_artesania/"
working_directory root
pid "#{root}/tmp/pids/unicorn.app1.pid"
listen "/tmp/unicorn.app1.sock"
stderr_path "/home/web/apps/web_artesania/tmp/logs/unicorn.app1_stderr.log"
stdout_path "/home/web/apps/web_artesania/tmp/logs/unicorn.app1_stdout.log"