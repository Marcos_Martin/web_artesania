#encoding: utf-8
class Article < ActiveRecord::Base
  attr_accessible :body, :title

  validates :title,:body, presence: true
  validates :title, uniqueness: true

  has_many :comments, :dependent => :destroy
end
