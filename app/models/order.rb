#encoding: utf-8
class Order < ActiveRecord::Base
  has_many :line_items, dependent: :destroy

  PAYMENT_TYPES = [ "Reembolso", "Transferencia Bancaria", "Paypal" ]
  ESTADO_PEDIDO = [ "Pendiente de pago", "Pago recibido, creando pedido", "Pedido enviado y finalizado"]

  validates :pay_type, inclusion: PAYMENT_TYPES
  validates :address, :email, :name, :telefono,:cp,:poblacion,:provincia,:pais, :presence => true
  validates_acceptance_of :aceptarCondiciones

  attr_accessible :address, :details, :email, :name, :pay_type ,:telefono,:localizador,:estadoPedido,
                  :cp,:poblacion,:provincia,:pais,:aceptarCondiciones

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end

end
