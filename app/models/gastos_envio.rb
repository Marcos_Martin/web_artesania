class GastosEnvio < ActiveRecord::Base
  attr_accessible :kilos, :precio, :tipo

  validates :kilos, :precio, :tipo, :presence => true
  validates_uniqueness_of :id
end
