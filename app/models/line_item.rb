#encoding: utf-8
class LineItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product
  belongs_to :cart
  # belongs_to le dice a Rails que lineas en la tabla line_items son hijas
  # de tuplas en las tablas carts y products. Una tupla line_item no puede
  # existir sin su correspondiente tupla en la tabla cart y product.

  attr_accessible :cart_id, :product_id

  def total_price
    product.price * quantity
  end
  def total_volumen
    product.volumen * quantity
  end
  def total_elements
    1*quantity
  end

end
