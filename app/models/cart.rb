#encoding: utf-8
class Cart < ActiveRecord::Base
  # attr_accessible :title, :body
  #has_many :line_items-> un carro tendra varios line items
  #dependent: :destroy->  si borramos un carro, se eliminaran toda tupla de
  #                       line_items asociada a ese carro. La asociacion esta
  #                       en el modelo line_items.rb
  has_many :line_items, dependent: :destroy

  def add_product(product_id)
    current_item = line_items.find_by_product_id(product_id)
    if current_item
      current_item.quantity += 1
    else
      current_item = line_items.build(product_id: product_id)
    end
    current_item
  end

  def cantidadItems
    line_items.to_a.sum { |item| item.total_elements }
  end

  def total_price
    line_items.to_a.sum { |item| item.total_price }
  end
  def total_volumen
    line_items.to_a.sum { |item| item.total_volumen }
  end
  def calculoGastosEnvio
    $pesoTotal=total_volumen
    $factorConversion=GastosEnvio.select(:precio).where('tipo LIKE "factorConversion"').first#255 #225 kg. /m3

    $volumenKG=$pesoTotal*$factorConversion.precio

    $busquedaPrecioRango=GastosEnvio.select(:precio).where("tipo LIKE 'rango' AND kilos>=#{$volumenKG}").order("precio ASC").first#255 #225 kg. /m3
    if ($busquedaPrecioRango==nil)
      $maximoPeso=GastosEnvio.maximum("kilos")
      $busquedaPrecioRango=GastosEnvio.select(:precio).where("tipo LIKE 'rango' AND kilos=#{$maximoPeso}").order("precio ASC").first#255 #225 kg. /m3
    end

    return $busquedaPrecioRango.precio
=begin
    if $volumenKG==0
      return 0
    elsif $volumenKG>0 && $volumenKG<=5
      return 4
    elsif $volumenKG>5 && $volumenKG<=10
      return 5
    elsif $volumenKG>10 && $volumenKG<=20
      return 7
    elsif $volumenKG>20 && $volumenKG<=30
      return 8
    elsif $volumenKG>30 && $volumenKG<=40
      return 9
    elsif $volumenKG>40 && $volumenKG<=50
      return 10
    elsif $volumenKG>50 && $volumenKG<=60
      return 12
    elsif $volumenKG>60 && $volumenKG<=70
      return 13
    elsif $volumenKG>70 && $volumenKG<=80
      return 14
    elsif $volumenKG>80 && $volumenKG<=90
      return 15
    elsif $volumenKG>90 && $volumenKG<=100
      return 16
    elsif $volumenKG>100 && $volumenKG<=110
      return 17
    elsif $volumenKG>110 && $volumenKG<=120
      return 19
    elsif $volumenKG>120 && $volumenKG<=130
      return 20
    elsif $volumenKG>130 && $volumenKG<=140
      return 22
    elsif $volumenKG>140 && $volumenKG<=150
      return 23
    elsif $volumenKG>150 && $volumenKG<=160
      return 24
    elsif $volumenKG>160 && $volumenKG<=170
      return 25
    elsif $volumenKG>170 && $volumenKG<=180
      return 27
    elsif $volumenKG>180 && $volumenKG<=190
      return 28
    elsif $volumenKG>190 && $volumenKG<=200
      return 29
    elsif $volumenKG>200 && $volumenKG<=225
      return 30
    elsif $volumenKG>225 && $volumenKG<=250
      return 32
    elsif $volumenKG>250 && $volumenKG<=275
      return 33
    elsif $volumenKG>275 && $volumenKG<=300
      return 34
    elsif $volumenKG>300
      return 35
    end
=end
  end
  def totalAPagar
    calculoGastosEnvio+total_price
  end
  def gastosReembolso
    $busquedaGastosReembolso=GastosEnvio.select(:precio).where('tipo LIKE "gastosRembolso"').first
    Rails.logger.error  "-------------------busquedaPrecioRango: "+$busquedaGastosReembolso.precio.to_s
    return $busquedaGastosReembolso.precio #expresado en €
  end

end
