#encoding: utf-8
class Product < ActiveRecord::Base
  TIPOS_PRODUCTOS = [ "Ceramica", "Pintura", "Jabones_naturales", "Serijos",
                      "Bisuteria", "Decoracion","Antiguedades","Muebles",
                      "Vinos", "Quesos", "Souvenires", "Varios"]
  attr_accessible :description, :extensionImagen,:extensionImagena,:extensionImagenb, :price, :tipo, :title, :photo,:photoa,:photob,:volumen
  validates :title, :description,:tipo,:volumen, presence: true
  validates_length_of :title ,:minimum => 5 , :maximum => 21
  validates :tipo, inclusion: TIPOS_PRODUCTOS
  validates :price, numericality: {greater_than_or_equal_to: 0.01}
  validates :title, uniqueness: true

  ##claves ajenas de base de datos
  has_many :line_items
  before_destroy :ensure_not_referenced_by_any_line_item


  after_save :guardar_foto
  after_destroy :eliminar_foto

  def photo=(file_data)
    unless file_data.blank?
      @file_data = file_data
      self.extensionImagen = file_data.original_filename.split('.').last.downcase
    end
  end
  def photoa=(file_data)
    unless file_data.blank?
      @file_dataa = file_data
      self.extensionImagena = file_data.original_filename.split('.').last.downcase
    end
  end
  def photob=(file_data)
    unless file_data.blank?
      @file_datab = file_data
      self.extensionImagena = file_data.original_filename.split('.').last.downcase
    end
  end

  def photo     #estafuncion es necesaria para que funcionen las validaciones. Hace como un getter
    unless @file_data.blank?
      photo_path
    else
      nil
    end
  end
  def photoa
    unless @file_dataa.blank?
      photoa_path
    else
      nil
    end
  end
  def photob
    unless @file_datab.blank?
      photob_path
    else
      nil
    end
  end



  def photo_filename
    File.join Rails.root,"public" ,"imagenes","productosFotos", "#{tipo}", "#{id}.#{extensionImagen}"
  end
  def photoa_filename
    File.join Rails.root,"public" ,"imagenes","productosFotos", "#{tipo}", "#{id}a.#{extensionImagena}"
  end
  def photob_filename
    File.join Rails.root,"public" ,"imagenes","productosFotos", "#{tipo}", "#{id}b.#{extensionImagena}"
  end


  def photo_path
    "/imagenes/productosFotos/#{tipo}/#{id}.#{extensionImagen}"
  end
  def photoa_path
    "/imagenes/productosFotos/#{tipo}/#{id}a.#{extensionImagena}"
  end
  def photob_path
    "/imagenes/productosFotos/#{tipo}/#{id}b.#{extensionImagena}"
  end


  def has_photo?
    File.exist? photo_filename
  end
  def has_photoa?
    File.exist? photoa_filename
  end
  def has_photob?
    File.exist? photob_filename
  end


  private
  def guardar_foto
    if @file_data
      FileUtils.mkdir_p File.join Rails.root,"public" ,"imagenes","productosFotos", "#{tipo}"
      File.open(photo_filename, 'wb') do |f|
        f.write(@file_data.read)
      end
      @file_data = nil
    end
    if @file_dataa
      FileUtils.mkdir_p File.join Rails.root,"public" ,"imagenes","productosFotos", "#{tipo}"
      File.open(photoa_filename, 'wb') do |f|
        f.write(@file_dataa.read)
      end
      @file_dataa = nil
    end
    if @file_datab
      FileUtils.mkdir_p File.join Rails.root,"public" ,"imagenes","productosFotos", "#{tipo}"
      File.open(photob_filename, 'wb') do |f|
        f.write(@file_datab.read)
      end
      @file_datab = nil
    end
  end

  def eliminar_foto
    File.delete(photo_filename)
    File.delete(photoa_filename)
    File.delete(photob_filename)
  end


  # nos aseguramos de que no hay line_items referenciando este producto
  def ensure_not_referenced_by_any_line_item
    if line_items.empty?
      return true
    else
      errors.add(:base, 'Line Items present')
      return false
    end
  end


end
