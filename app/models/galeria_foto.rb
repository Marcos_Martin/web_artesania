#encoding: utf-8
class GaleriaFoto < ActiveRecord::Base
  attr_accessible :descripcion, :titulo, :photo
  FOTOS = File.join Rails.root,'public' ,'imagenes','galeriaFotos'
  validates :descripcion, :titulo, presence: true
  after_save :guardar_foto
  after_destroy :eliminar_foto
  #after_update :actualizar_foto
  def photo=(file_data)
    unless file_data.blank?
      @file_data = file_data
      self.formato = file_data.original_filename.split('.').last.downcase
    end
  end

  def photo_filename
    File.join FOTOS, "#{id}.#{formato}"
  end

  def photo_path
    "/imagenes/galeriaFotos/#{id}.#{formato}"
  end

  def has_photo?
    File.exist? photo_filename
  end

  private
  def guardar_foto
    if @file_data
      FileUtils.mkdir_p FOTOS
      File.open(photo_filename, 'wb') do |f|
        f.write(@file_data.read)

      end
      @file_data = nil
    end
  end

  def eliminar_foto
    File.delete(photo_filename)
  end



end
