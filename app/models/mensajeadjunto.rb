#encoding: utf-8
class Mensajeadjunto
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming

  attr_accessor :name, :email, :subject, :body,:telefono,:archivo
  validates :name, :email, :subject, :body,:telefono, :presence => true
  validates :email, :format => { :with => %r{.+@.+\..+} }, :allow_blank => true
  validate :validates_uploadfile

  def validates_uploadfile()
    max_size = 20971520  #bytes de 20Mbytes (correo de gmail max=25Mbytes)
    #177891bytes =173KB de windows
    if(archivo != nil)
      Rails.logger.info("------### TAMANYO ARCHIVO: "+archivo.size.to_s)
      errors.add(:archivo, "Archivo demasiado grande")if archivo.size > max_size
    end

  end

  def initialize(attributes = {})
    attributes.each do |name, value|
      send("#{name}=", value)
    end
  end

  def persisted?
    false
  end

end
