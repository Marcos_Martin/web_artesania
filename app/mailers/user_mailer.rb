# encoding: utf-8
class UserMailer < ActionMailer::Base
  default from: 'En LaMancha Online<enlamanchaonline@gmail.com>'

  def password_reset(user)
    @user = user
    mail :to => user.email, :subject => "En LaMancha Online - Cambio de Contraseña"
  end
end