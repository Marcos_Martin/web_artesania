#encoding: utf-8
class OrderNotifier < ActionMailer::Base
  default from: 'Web Artesania <borlom@gmail.com>'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.received.subject
  #
  def received(order)
    @order = order
    mail to: order.email, subject: 'La Mancha Online - Confirmacion de Compra'
  end


  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.order_notifier.shipped.subject
  #
  def shipped(order) #informacion de pedido nuevo para el admin
    @order = order
    mail to: "En LaMancha Online<enlamanchaonline@gmail.com>", subject: 'En LaMancha Online - NUEVO ENCARGO!', from: order.email
  end

  def cambioEstado(order) #informacion de cambio de estado para el cliente
    @order = order
    mail to: order.email, subject: 'En LaMancha Online - Cámbio de estado en su pedido Cod. '+order.id.to_s+'.'
  end


end
