class UserNotifier < ActionMailer::Base
  default from: 'En LaMancha Online<enlamanchaonline@gmail.com>'

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_notifier.nuevoUsuario.subject
  #
  def nuevoUsuario(user)
    @user=user
    mail to: user.email  , subject: 'En LaMancha Online - Nuevo Usuario'
  end
end
