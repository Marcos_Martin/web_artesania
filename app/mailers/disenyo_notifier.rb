# encoding: utf-8
class DisenyoNotifier < ActionMailer::Base
  default from: "En LaMancha Online<juanito@gmail.com>"
  def pedido(message)
    @message=message
    file=message.archivo
    unless file.blank?
      attachments[file.original_filename] = File.open(file.path, 'rb'){|f| f.read}
    end
    mail to: "En LaMancha Online<enlamanchaonline@gmail.com>", subject: 'En LaMancha Online - NUEVO ENCARGO! Consulta de Diseño o Restauración', from: message.email.to_s
  end
end
