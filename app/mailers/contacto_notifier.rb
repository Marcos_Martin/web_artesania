# encoding: utf-8
class ContactoNotifier < ActionMailer::Base
  default from: "En LaMancha Online<enlamanchaonline@gmail.com>"

  def contactar(message)
    @message=message
    mail to: "En LaMancha Online<enlamanchaonline@gmail.com>", subject: 'En LaMancha Online - Mail de contacto',from: message.email
  end
end
