# encoding: utf-8
class SessionsController < ApplicationController
  skip_before_filter :authorize,:filtro_admin
  def new
    session[:intentos_accesos]=0 if session[:intentos_accesos].nil?
  end

  def create

    #intentos_accesos podria controlar en el futuro un numero maximo de intentos login
    unless session[:intentos_accesos]
      then session[:intentos_accesos]=0
    end
    user = User.find_by_name(params[:name])
    if session[:intentos_accesos]>0
      if user and user.authenticate(params[:password]) and verify_recaptcha
        if params[:remember_me]
          cookies.permanent[:auth_token] = user.auth_token
        else
          cookies[:auth_token] = user.auth_token
        end
        session[:intentos_accesos]=0
        if user.tipo == "xerif"
          redirect_to admin_url,notice: "HOLA Jefe :)"
        else
          redirect_to store_url, notice: "Bienvenido "+user.name+" :)"
        end

      else #fallo inicio de sesion
          session[:intentos_accesos]+=1
          if verify_recaptcha
            redirect_to login_url,notice: "Combinación invalida de usuario/contraseña. "+ session[:intentos_accesos].to_s+" intento/s de acceso"
          else
            redirect_to login_url,notice: "Te has equivocado con el Captcha, intentalo otra vez. Van "+ session[:intentos_accesos].to_s+" intento/s de acceso"
          end
      end
    else
      if session[:intentos_accesos]==0
        if user and user.authenticate(params[:password])
          if params[:remember_me]
            cookies.permanent[:auth_token] = user.auth_token
          else
            cookies[:auth_token] = user.auth_token
          end
          session[:intentos_accesos]=0
          if user.tipo == "xerif"
            redirect_to admin_url,notice: "HOLA Jefe :)"
          else
            redirect_to store_url, notice: "Bienvenido "+user.name+" :)"
          end

        else #fallo inicio de sesion
          session[:intentos_accesos]+=1
          redirect_to login_url,notice: "Combinación invalida de usuario/contraseña. "+ session[:intentos_accesos].to_s+" intento/s de acceso"
        end
      end
    end














    #
    #if user and user.authenticate(params[:password])
    #  if params[:remember_me]
    #    cookies.permanent[:auth_token] = user.auth_token
    #  else
    #    cookies[:auth_token] = user.auth_token
    #  end
    #  session[:intentos_accesos]=0
    #  if user.tipo == "xerif"
    #    redirect_to admin_url,notice: "HOLA Jefe :)"
    #  else
    #    redirect_to store_url, notice: "Bienvenido "+user.name+" :)"
    #  end
    #
    #else #fallo inicio de sesion
    #  session[:intentos_accesos]+=1
    #  redirect_to login_url,notice: "Combinación invalida de usuario/contraseña. "+ session[:intentos_accesos].to_s+" intento/s de acceso"
    #end
    #





  end

  def destroy

    cookies.delete(:auth_token)
    @cart = current_cart
    @cart.destroy
    session[:cart_id] = nil
    redirect_to store_url, notice: "¿Ya te vas? ب_ب"
  end
end
