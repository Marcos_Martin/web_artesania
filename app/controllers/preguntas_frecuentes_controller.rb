#encoding: utf-8
class PreguntasFrecuentesController < ApplicationController
  skip_before_filter :authorize,:filtro_admin
  def index
    @tituloPagina="Preguntas frecuentes"
    #---- ejemplo gastos de envio
    @tabla_gastos_envio = GastosEnvio.where("tipo LIKE 'rango'")
    @producto1_volumen=0.123 #m cubicos
    @factor_conversion=(GastosEnvio.select(:precio).where('tipo LIKE "factorConversion"').first).precio#255 #225 kg. /m3
    @gastos_reembolso=(GastosEnvio.select(:precio).where('tipo LIKE "gastosRembolso"').first).precio#255 #225 kg. /m3
    $volumenKG=@producto1_volumen*@factor_conversion
    $busquedaPrecioRango=GastosEnvio.select(:precio).where("tipo LIKE 'rango' AND kilos>=#{$volumenKG}").order("precio ASC").first#255 #225 kg. /m3
    if ($busquedaPrecioRango==nil)
      $maximoPeso=GastosEnvio.maximum("kilos")
      $busquedaPrecioRango=GastosEnvio.select(:precio).where("tipo LIKE 'rango' AND kilos=#{$maximoPeso}").order("precio ASC").first#255 #225 kg. /m3
    end
    @precioRango=$busquedaPrecioRango.precio
    #---- ejemplo gastos de envio
  end
end
