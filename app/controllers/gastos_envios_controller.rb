# encoding: utf-8
class GastosEnviosController < ApplicationController
  skip_before_filter :authorize , only: []
  skip_before_filter :filtro_admin , only: []
  # GET /gastos_envios
  # GET /gastos_envios.json
  def index
    @gastos_envios = GastosEnvio.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @gastos_envios }
    end
  end

  # GET /gastos_envios/1
  # GET /gastos_envios/1.json
  def show
    @gastos_envio = GastosEnvio.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @gastos_envio }
    end
  end

  # GET /gastos_envios/new
  # GET /gastos_envios/new.json
  def new
    @gastos_envio = GastosEnvio.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @gastos_envio }
    end
  end

  # GET /gastos_envios/1/edit
  def edit
    @gastos_envio = GastosEnvio.find(params[:id])
  end

  # POST /gastos_envios
  # POST /gastos_envios.json
  def create
    @gastos_envio = GastosEnvio.new(params[:gastos_envio])
    respond_to do |format|
      if @gastos_envio.save
        format.html { redirect_to gastos_envios_path, notice: 'Gastos envio fue creado con éxito.' }
        format.json { render json: @gastos_envio, status: :created, location: @gastos_envio }
      else
        format.html { render action: "new" }
        format.json { render json: @gastos_envio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /gastos_envios/1
  # PUT /gastos_envios/1.json
  def update
    @gastos_envio = GastosEnvio.find(params[:id])
    respond_to do |format|
      if @gastos_envio.update_attributes(params[:gastos_envio])
        format.html { redirect_to gastos_envios_path, notice: 'Gastos envio fue editado con éxito.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @gastos_envio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /gastos_envios/1
  # DELETE /gastos_envios/1.json
  def destroy
    @gastos_envio = GastosEnvio.find(params[:id])
    @gastos_envio.destroy
    respond_to do |format|
      format.html { redirect_to gastos_envios_url }
      format.json { head :no_content }
    end
  end
  def resetearValores
    ActiveRecord::Base.connection.execute("DELETE FROM gastos_envios")
    #ActiveRecord::Base.connection.execute("delete from sqlite_sequence where name = 'gastos_envios'")
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 0, :precio => 0
    GastosEnvio.create :id=>2, :tipo => "rango", :kilos => 0.01, :precio => 4
    GastosEnvio.create :id=>3, :tipo => "rango", :kilos => 5, :precio => 4
    GastosEnvio.create :id=>4, :tipo => "rango", :kilos => 10, :precio => 5
    GastosEnvio.create :id=>5, :tipo => "rango", :kilos => 20, :precio => 7
    GastosEnvio.create :id=>6, :tipo => "rango", :kilos => 30, :precio => 8
    GastosEnvio.create :id=>7, :tipo => "rango", :kilos => 40, :precio => 9
    GastosEnvio.create :id=>8, :tipo => "rango", :kilos => 50, :precio => 10
    GastosEnvio.create :id=>9, :tipo => "rango", :kilos => 60, :precio => 12
    GastosEnvio.create :id=>10, :tipo => "rango", :kilos => 70, :precio => 13
    GastosEnvio.create :id=>11, :tipo => "rango", :kilos => 80, :precio => 14
    GastosEnvio.create :id=>12, :tipo => "rango", :kilos => 90, :precio => 15
    GastosEnvio.create :id=>13, :tipo => "rango", :kilos => 100, :precio => 16
    GastosEnvio.create :id=>14, :tipo => "rango", :kilos => 110, :precio => 17
    GastosEnvio.create :id=>15, :tipo => "rango", :kilos => 120, :precio => 19
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 130, :precio => 20
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 140, :precio => 22
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 150, :precio => 23
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 160, :precio => 24
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 170, :precio => 25
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 180, :precio => 27
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 190, :precio => 28
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 200, :precio => 29
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 225, :precio => 30
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 250, :precio => 32
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 275, :precio => 33
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 300, :precio => 34
    GastosEnvio.create :id=>1, :tipo => "factorConversion", :kilos => -1, :precio => 225
    GastosEnvio.create :id=>1, :tipo => "gastosRembolso", :kilos => -1, :precio => 8
    redirect_to gastos_envios_path, notice: "Gastos de envio como al principio! OK"
  end
  def gastosEnvioCero
    ActiveRecord::Base.connection.execute("DELETE FROM gastos_envios")
    #ActiveRecord::Base.connection.execute("delete from sqlite_sequence where name = 'gastos_envios'")
    GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 0, :precio => 0
    GastosEnvio.create :id=>1, :tipo => "factorConversion", :kilos => -1, :precio => 225
    GastosEnvio.create :id=>1, :tipo => "gastosRembolso", :kilos => -1, :precio => 8
    redirect_to gastos_envios_path, notice: "Sin gastos de envio. Solo tiene el reembolso! OK"
  end
end
