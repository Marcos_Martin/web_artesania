# encoding: utf-8
class DisenyoController < ApplicationController
  skip_before_filter :authorize,:filtro_admin
  def new
    @tituloPagina="Diseño y Restauración"
    @message = Mensajeadjunto.new
    flash[:notice]=nil
    @galeria_fotos = GaleriaFoto.paginate page: params[:page], order: 'created_at desc', per_page: 9
  end

  def create
    @message = Mensajeadjunto.new(params[:mensajeadjunto])
    @galeria_fotos = GaleriaFoto.paginate page: params[:page], order: 'created_at desc', per_page: 9
    if @message.valid?
      DisenyoNotifier.pedido(@message).deliver
      redirect_to(disenyo_path, :notice => "El mensaje se envio correctamente.")
    else
      flash[:notice] = "Revise los campos, no se envio"
      render :new
    end
  end
end
