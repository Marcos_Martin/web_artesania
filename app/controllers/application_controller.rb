# encoding: utf-8
class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :authorize, :logueado, :elementos_carrito, :filtro_admin

  protected
  def authorize
    user=loginActual
    unless user!=nil and User.find_by_id(user.id)
      redirect_to login_url, notice: "Hola, ¿Quén eres?"
    end
  end

  def filtro_admin
    user=loginActual
    unless user!=nil and user.tipo=="xerif"
      redirect_to login_url, notice: "Acceso no permitido, identificate como Administrador"
    end
  end


  def loginActual
    #user = User.find_by_id(session[:user_id])
    user ||= User.find_by_auth_token( cookies[:auth_token]) if cookies[:auth_token]
  end

  def logueado
    @logueado ||= User.find_by_auth_token( cookies[:auth_token]) if cookies[:auth_token]
  end


  def current_cart
      Cart.find(session[:cart_id])
    rescue ActiveRecord::RecordNotFound
      cart = Cart.create
      session[:cart_id] = cart.id
      cart
  end

  def elementos_carrito
    cart=current_cart
    @nItems=cart.cantidadItems
    return @nItems
  end
  def limpiarBBDD
    #En order#new, se limpia automatica/ cuando alguien hace un pedido
    #Ademas de la funcion del administrador
    limite=Date.today - 1.day
    #se limpian las entradas de ante ayer en adelante porque alguien  puede estar comprando
    #a las 23:59
    LineItem.delete_all("order_id Is Null AND updated_at < '#{limite}'")
    Cart.delete_all("created_at < '#{limite}'")
    #limpieza BD
  end


end
