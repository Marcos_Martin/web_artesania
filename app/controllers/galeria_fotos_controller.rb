#encoding: utf-8
class GaleriaFotosController < ApplicationController
  skip_before_filter :authorize , only: [:verGaleriaFotos,:show]
  skip_before_filter :filtro_admin , only: [:verGaleriaFotos,:show]
  # GET /galeria_fotos
  # GET /galeria_fotos.json
  def index
    @galeria_fotos = GaleriaFoto.paginate page: params[:page], order: 'created_at desc', per_page: 15
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @galeria_fotos }
    end
  end

  # GET /galeria_fotos/1
  # GET /galeria_fotos/1.json
  def show
    @galeria_foto = GaleriaFoto.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @galeria_foto }
    end
  end

  # GET /galeria_fotos/new
  # GET /galeria_fotos/new.json
  def new
    @galeria_foto = GaleriaFoto.new
    @tituloPagina="Galeria Imagenes - Subir imagen"
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @galeria_foto }
    end
  end

  # GET /galeria_fotos/1/edit
  def edit
    @galeria_foto = GaleriaFoto.find(params[:id])
    @tituloPagina="Galeria Imagenes - Editar imagen: "+@galeria_foto.titulo
  end

  # POST /galeria_fotos
  # POST /galeria_fotos.json
  def create
    @galeria_foto = GaleriaFoto.new(params[:galeria_foto])
    respond_to do |format|
      if @galeria_foto.save
        format.html { redirect_to new_galeria_foto_path, notice: 'Galeria foto was successfully created.' }
        format.json { render json: @galeria_foto, status: :created, location: @galeria_foto }
      else
        format.html { render action: "new" }
        format.json { render json: @galeria_foto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /galeria_fotos/1
  # PUT /galeria_fotos/1.json
  def update
    @galeria_foto = GaleriaFoto.find(params[:id])
    respond_to do |format|
      if @galeria_foto.update_attributes(params[:galeria_foto])
        format.html { redirect_to galeria_fotos_url, notice: 'Imagen de la galeria actualizada. Si no se ve es por la cache del navegador' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @galeria_foto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /galeria_fotos/1
  # DELETE /galeria_fotos/1.json
  def destroy
    @galeria_foto = GaleriaFoto.find(params[:id])
    @galeria_foto.destroy
    respond_to do |format|
      format.html { redirect_to galeria_fotos_url }
      format.json { head :no_content }
    end
  end

  def verGaleriaFotos
    @tituloPagina="GALERIA FOTOS"
    @galeria_fotos = GaleriaFoto.paginate page: params[:page], order: 'created_at desc', per_page: 8
    #@orders = Order.paginate page: params[:page], order: 'created_at desc', per_page: 10
    respond_to do |format|
      format.html
      format.json { render json: @galeria_fotos }
    end
  end

  def busqueda
    @tituloPagina ="Lista de diseños personalizados - Busqueda"
    @galeria_fotos = GaleriaFoto.paginate :conditions => ['titulo LIKE ?',"%#{params[:search]}%"],page: params[:page], order: 'created_at desc', per_page: 15
    render "index"
  end

end
