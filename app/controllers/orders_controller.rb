#encoding: utf-8
class OrdersController < ApplicationController
  skip_before_filter :authorize, only: []
  skip_before_filter :filtro_admin, only: [:new,:create]
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.paginate page: params[:page], order: 'created_at desc',per_page: 15
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    @order = Order.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/new
  # GET /orders/new.json
  def new
    @cart = current_cart
    @tituloPagina="Complete los datos para finalizar"
    if @cart.line_items.empty?
      redirect_to store_url, notice: "No es posibler realizar el pedido porque su carrito esta vacio"
      return
    end
    @order = Order.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @order }
    end
  end

  # GET /orders/1/edit
  def edit
    @order = Order.find(params[:id])
    @cart = current_cart
    @tituloPagina="Editar pedido con código "+@order.id.to_s
  end

  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(params[:order])
    @order.usuario= @logueado.name #logueado esta declarado en un before_filter en application_controller
    @order.add_line_items_from_cart(current_cart)
    @order.precioTotal=current_cart.total_price
    @order.gastosEnvio=current_cart.calculoGastosEnvio
    if @order.pay_type=="Reembolso"
      @order.gastosEnvio=current_cart.calculoGastosEnvio+current_cart.gastosReembolso
    end

    respond_to do |format|
      if @order.save
        if @order.pay_type=="Reembolso"
          @order.update_column :estadoPedido, "Creando pedido"
        end
        Cart.destroy(session[:cart_id])
        session[:cart_id] = nil
        #limpieza BD (line_items y carts)
        limpiarBBDD
        #limpieza BD
        OrderNotifier.received(@order).deliver
        OrderNotifier.shipped(@order).deliver
        format.html { redirect_to store_url, notice: "Gracias por su compra.<br/><br/>Revise su email "+@order.email+" para más información o su #{view_context.link_to('sección de usuario', '/users/areaUsuario')}".html_safe}
        format.json { render json: @order, status: :created, location: @order }
      else
        @cart = current_cart
        format.html { render action: "new" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /orders/1
  # PUT /orders/1.json
  def update
    @order = Order.find(params[:id])
    respond_to do |format|
      if @order.update_attributes(params[:order])
        if @order.estadoPedido=="Pago recibido, creando pedido" || @order.estadoPedido=="Pedido enviado y finalizado"
          OrderNotifier.cambioEstado(@order).deliver
        end
        format.html { redirect_to orders_path, notice: 'Order was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @order.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy

    @order = Order.find(params[:id])
    @order.destroy

    respond_to do |format|
      format.html { redirect_to orders_url }
      format.json { head :no_content }
    end
  end
  def busqueda
    case params[:subject]
      when "Por código"
        @orders = Order.paginate :conditions => ['id LIKE ?',"%#{params[:search]}%"],page: params[:page], order: 'created_at desc', per_page: 15
      when "Por nombre"
        @orders = Order.paginate :conditions => ['name LIKE ?',"%#{params[:search]}%"],page: params[:page], order: 'created_at desc', per_page: 15
      when "Por email"
        @orders = Order.paginate :conditions => ['email LIKE ?',"%#{params[:search]}%"],page: params[:page], order: 'created_at desc', per_page: 15
      else
        @orders = Order.paginate page: params[:page], order: 'created_at desc',per_page: 15
    end
    render "index"
  end
end
