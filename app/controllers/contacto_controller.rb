# encoding: utf-8
class ContactoController < ApplicationController
  skip_before_filter :authorize,:filtro_admin
  def new
    @tituloPagina="CONTACTO"
    @message = Message.new
    flash[:notice]=nil
  end

  def create
    @tituloPagina="CONTACTO"
    @message = Message.new(params[:message])
    if @message.valid?
      ContactoNotifier.contactar(@message).deliver
      redirect_to(contacto_path, :notice => "El mensaje se envio correctamente")
    else
      flash[:notice] = "Por favor, revise los campos. No se envio."
      render :new
    end
  end
end
