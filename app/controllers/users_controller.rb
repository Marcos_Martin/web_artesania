#encoding: utf-8
class UsersController < ApplicationController
  skip_before_filter :authorize, only: [:new,:create ]
  skip_before_filter :filtro_admin, only: [:new,:create,:edit,:update,:areaUsuario]
  # GET /users
  # GET /users.json
  def index

    @users = User.paginate page: params[:page], order: 'created_at desc', per_page: 15
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show

    @user = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new

    @user = User.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    @tituloPagina="Editar usuario: "+@user.name
    if(@user.email != @logueado.email)
      if(@logueado.tipo != "xerif")
        redirect_to store_path,notice:"Acceso denegado"
      end
    end
  end

  # POST /users
  # POST /users.json
  def create

    @user = User.new(params[:user])
    unless @logueado != nil and @logueado.tipo=="xerif"
      @user.tipo="usuario"
    end #el Xerif es el unico q puede editar el campo usuario
    respond_to do |format|
      if @logueado != nil and @logueado.tipo=="xerif"
        if  @user.save
          user_recien_salvado = User.find_by_name(@user.name)
          #session[:user_id] = user_recien_salvado.id
          cookies[:auth_token] = user_recien_salvado.auth_token     ##al crearlo queda logueado pero no permanentemente
          UserNotifier.nuevoUsuario(@user).deliver
          format.html { redirect_to store_path,
                                    notice: "Usuario #{@user.name} creado con exito. Ya puede comprar o comentar noticias" }
          format.json { render json: @user, status: :created, location: @user }
        else
          format.html { render action: "new" }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end

      else
        if verify_recaptcha and @user.save
          user_recien_salvado = User.find_by_name(@user.name)
          #session[:user_id] = user_recien_salvado.id
          cookies[:auth_token] = user_recien_salvado.auth_token     ##al crearlo queda logueado pero no permanentemente
          UserNotifier.nuevoUsuario(@user).deliver
          format.html { redirect_to store_path,
                                    notice: "Usuario #{@user.name} creado con exito. Ya puede comprar o comentar noticias" }
          format.json { render json: @user, status: :created, location: @user }
        else
          unless verify_recaptcha
            @user.errors[:base] << "Error captcha"
          end
          format.html { render action: "new" }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = User.new(params[:user])
    if @logueado.tipo!="xerif"
      @user.tipo="usuario"
    end #el Xerif es el unico q puede editar el campo usuario
    @user = User.find(params[:id])
    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to "/users/areaUsuario",
                                  notice: "El usuario #{@user.name} se actualizó con éxito." }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy

    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  def areaUsuario

    @tituloPagina="Area de usuario"
    @usuarioActual=loginActual
    @orders=nil
    @carts=nil
    if @usuarioActual
      @orders = Order.paginate :conditions => { :email => @usuarioActual.email },page: params[:page], order: 'created_at desc', per_page: 6
    else
      redirect_to login_path, :notice=> "Identifiquese para acceder a su área de usuario"
    end
  end

  def busqueda
    case params[:subject]
      when "Por teléfono"
        @users = User.paginate :conditions => ['telefono LIKE ?',"%#{params[:search]}%"],page: params[:page], order: 'created_at desc', per_page: 15
      when "Por nombre"
        @users = User.paginate :conditions => ['name LIKE ?',"%#{params[:search]}%"],page: params[:page], order: 'created_at desc', per_page: 15
      when "Por email"
        @users = User.paginate :conditions => ['email LIKE ?',"%#{params[:search]}%"],page: params[:page], order: 'created_at desc', per_page: 15
      else
        @users = User.paginate page: params[:page], order: 'created_at desc',per_page: 15
    end
    render "index"
  end
end
