#encoding: utf-8
class BlogController < ApplicationController
  skip_before_filter :authorize,:filtro_admin
  def index
    @articles = Article.paginate page: params[:page], order: 'created_at desc', per_page: 3
  end

  def new  #crear un comentario desde blog
    @comment = Comment.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @comment }
    end
  end

  def create  #crear un comentario desde blog
    datos= params[:comment]
    @article = Article.find("#{datos.id}")
    datos.delete(:id)
    @comment = @article.comments.build(datos)
    respond_to do |format|
      if @comment.save
        format.html { redirect_to(@article, :notice => 'Comentario creado correctamente') }
        format.xml  { render :xml => @article, :status => :created, :location => @article }
      else
        format.html { redirect_to(@article, :notice =>
            'Comment could not be saved. Please fill in all fields')}
        format.xml  { render :xml => @comment.errors, :status => :unprocessable_entity }
      end
    end
  end
end
