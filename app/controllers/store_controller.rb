# encoding: utf-8
class StoreController < ApplicationController
  skip_before_filter :authorize,:filtro_admin
  def index
    @tituloPagina ="Tienda - Todos los productos"
    @products = Product.paginate page: params[:page], order: 'created_at desc', per_page: 6
    @cart=current_cart
  end

  def ceramica
    @tituloPagina ="Tienda - Cerámica"
    @products = Product.paginate :conditions => { :tipo => 'Ceramica' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def pintura
    @tituloPagina ="Tienda - Pintura"
    @products = Product.paginate :conditions => { :tipo => 'Pintura' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def jabones_naturales
    @tituloPagina ="Tienda - Jabones naturales"
    @products = Product.paginate :conditions => { :tipo => 'Jabones_naturales' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def serijos
    @tituloPagina ="Tienda - Serijos"
    @products = Product.paginate :conditions => { :tipo => 'Serijos' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def bisuteria
    @tituloPagina ="Tienda - Bisuteria"
    @products = Product.paginate :conditions => { :tipo => 'Bisuteria' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def decoracion
    @tituloPagina ="Tienda - Serijos"
    @products = Product.paginate :conditions => { :tipo => 'Serijos' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def antiguedades
    @tituloPagina ="Tienda - Antigüedades"
    @products = Product.paginate :conditions => { :tipo => 'Antiguedades' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def muebles
    @tituloPagina ="Tienda - Muebles"
    @products = Product.paginate :conditions => { :tipo => 'Muebles' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def vinos
    @tituloPagina ="Tienda - Vinos"
    @products = Product.paginate :conditions => { :tipo => 'Vinos' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def quesos
    @tituloPagina ="Tienda - Quesos"
    @products = Product.paginate :conditions => { :tipo => 'Quesos' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def souvenires
    @tituloPagina ="Tienda - Souvenires"
    @products = Product.paginate :conditions => { :tipo => 'Souvenires' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def varios
    @tituloPagina ="Tienda - Varios"
    @products = Product.paginate :conditions => { :tipo => 'Varios' },page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end

  def busqueda
    @tituloPagina ="Tienda - Busqueda"
    @products = Product.paginate :conditions => ['title LIKE ?',"%#{params[:search]}%"],page: params[:page], order: 'created_at desc', per_page: 15
    @cart=current_cart
  end
end
