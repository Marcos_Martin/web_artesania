#encoding: utf-8
class InicioController < ApplicationController
  skip_before_filter :authorize, :filtro_admin

  def index
    @tituloPagina="BIENVENIDO";
    @carrusel=Product.limit(10);
  end
end
