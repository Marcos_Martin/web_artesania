# encoding: utf-8
class TinymceAssetsController < ApplicationController
  skip_before_filter :authorize,:filtro_admin
  #skip_before_filter :filtro_admin, only[] #este plugin solo es para el admin
  def create
    # si se sube una imagen existente, no se reescribe y se muestra la ya grabada
    # si la imagen a subir no es jpg, gif o png no se sube.

    name = params[:file].original_filename
    extension= name.split('.').last.downcase.to_s
    directory = "public/imagenes/extras"
    rutaImagen= "/imagenes/extras/"+name
    path = File.join(directory, name)
    FileUtils.mkdir_p directory

    if not File.exist? (path)
      File.open(path, "wb") { |f| f.write(params[:file].read)}
    end

    if extension.eql?("jpg") or extension.eql?("gif") or extension.eql?("png")
      render json: {
          image: {
              url: rutaImagen
          }
      }, content_type: "text/html"
    else
      render json: {
          error: {
              message: "Extensión incorrecta"
          }
      }, content_type: "text/html"
    end

  end
end
