#encoding: utf-8
class ProductsController < ApplicationController
  skip_before_filter :authorize
  skip_before_filter :filtro_admin, only: [:show]
  # GET /products
  # GET /products.json
  def index
    @products = Product.paginate page: params[:page], order: 'created_at desc',per_page: 15
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @products }
    end
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @product = Product.find(params[:id])
    @tituloPagina ="Tienda - Detalles de "+@product.title
    @cart=current_cart
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/new
  # GET /products/new.json
  def new
    @product = Product.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @product }
    end
  end

  # GET /products/1/edit
  def edit
    @product = Product.find(params[:id])
    @tituloPagina= "Productos - Editar producto: "+@product.title.to_s
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(params[:product])
    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render json: @product, status: :created, location: @product }
      else
        format.html { render action: "new" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /products/1
  # PUT /products/1.json
  def update
    @product = Product.find(params[:id])
    respond_to do |format|
      if @product.update_attributes(params[:product])
        format.html { redirect_to products_url, notice: 'Producto actualizado. Si no se ve es por la cache del navegador' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product = Product.find(params[:id])
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url }
      format.json { head :no_content }
    end
  end

  def busqueda
    @products = Product.paginate :conditions => ['title LIKE ?',"%#{params[:search]}%"],page: params[:page], order: 'created_at desc', per_page: 15
    render "index"
  end
end
