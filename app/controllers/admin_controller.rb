# encoding: utf-8
class AdminController < ApplicationController
  skip_before_filter :authorize, only: []
  skip_before_filter :filtro_admin, only: []
  def index
    @tituloPagina="Zona de Administración"
    limpiarBBDD
  end

  def limpiezaBD
    limpiarBBDD
    redirect_to admin_path, notice: "Base de datos LIMPITA"
  end
end

