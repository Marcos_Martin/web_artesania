tinyMCE.addI18n('es.uploadimage_dlg', {
    title: 'Insertar imagen que tengas guardada',
    header: "Insertar imagen",
    input:  "Elige una imagen que tengas",
    uploading: "Subiendo imagen...",
    blank_input: "Debes elegir un archivo",
    bad_response: "Upss, el servidor no respondio bien",
    blank_response: "Upss, el servidor no respondio",
    insert: "OK",
    cancel: "Cancelar"
});