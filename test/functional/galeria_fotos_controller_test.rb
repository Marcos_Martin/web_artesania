require 'test_helper'

class GaleriaFotosControllerTest < ActionController::TestCase
  setup do
    @galeria_foto = galeria_fotos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:galeria_fotos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create galeria_foto" do
    assert_difference('GaleriaFoto.count') do
      post :create, galeria_foto: { descripcion: @galeria_foto.descripcion, formato: @galeria_foto.formato, titulo: @galeria_foto.titulo }
    end

    assert_redirected_to galeria_foto_path(assigns(:galeria_foto))
  end

  test "should show galeria_foto" do
    get :show, id: @galeria_foto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @galeria_foto
    assert_response :success
  end

  test "should update galeria_foto" do
    put :update, id: @galeria_foto, galeria_foto: { descripcion: @galeria_foto.descripcion, formato: @galeria_foto.formato, titulo: @galeria_foto.titulo }
    assert_redirected_to galeria_foto_path(assigns(:galeria_foto))
  end

  test "should destroy galeria_foto" do
    assert_difference('GaleriaFoto.count', -1) do
      delete :destroy, id: @galeria_foto
    end

    assert_redirected_to galeria_fotos_path
  end
end
