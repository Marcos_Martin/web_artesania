require 'test_helper'

class DisenyoRestauracionTest < ActionMailer::TestCase
  test "pedido" do
    mail = DisenyoRestauracion.pedido
    assert_equal "Pedido", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
