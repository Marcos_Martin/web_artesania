require 'test_helper'

class UserNotifierTest < ActionMailer::TestCase
  test "nuevoUsuario" do
    mail = UserNotifier.nuevoUsuario
    assert_equal "Nuevousuario", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
