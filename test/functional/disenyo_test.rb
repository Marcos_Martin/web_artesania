require 'test_helper'

class DisenyoTest < ActionMailer::TestCase
  test "pedido" do
    mail = Disenyo.pedido
    assert_equal "Pedido", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
