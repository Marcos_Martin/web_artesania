require 'test_helper'

class GastosEnviosControllerTest < ActionController::TestCase
  setup do
    @gastos_envio = gastos_envios(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gastos_envios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gastos_envio" do
    assert_difference('GastosEnvio.count') do
      post :create, gastos_envio: { kilos: @gastos_envio.kilos, precio: @gastos_envio.precio, tipo: @gastos_envio.tipo }
    end

    assert_redirected_to gastos_envio_path(assigns(:gastos_envio))
  end

  test "should show gastos_envio" do
    get :show, id: @gastos_envio
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gastos_envio
    assert_response :success
  end

  test "should update gastos_envio" do
    put :update, id: @gastos_envio, gastos_envio: { kilos: @gastos_envio.kilos, precio: @gastos_envio.precio, tipo: @gastos_envio.tipo }
    assert_redirected_to gastos_envio_path(assigns(:gastos_envio))
  end

  test "should destroy gastos_envio" do
    assert_difference('GastosEnvio.count', -1) do
      delete :destroy, id: @gastos_envio
    end

    assert_redirected_to gastos_envios_path
  end
end
