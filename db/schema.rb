# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130602172405) do

  create_table "articles", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "blogs", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "carts", :force => true do |t|
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "comments", :force => true do |t|
    t.integer  "article_id"
    t.string   "name"
    t.string   "email"
    t.text     "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "galeria_fotos", :force => true do |t|
    t.string   "titulo"
    t.string   "descripcion"
    t.string   "formato"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "gastos_envios", :force => true do |t|
    t.string   "tipo",                                     :default => "rango"
    t.decimal  "kilos",      :precision => 8, :scale => 2, :default => 0.0
    t.decimal  "precio",     :precision => 8, :scale => 2, :default => 0.0
    t.datetime "created_at",                                                    :null => false
    t.datetime "updated_at",                                                    :null => false
  end

  create_table "line_items", :force => true do |t|
    t.integer  "product_id"
    t.integer  "cart_id"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
    t.integer  "quantity",   :default => 1
    t.integer  "order_id"
  end

  create_table "orders", :force => true do |t|
    t.string   "name"
    t.text     "address"
    t.string   "email"
    t.string   "pay_type"
    t.text     "details"
    t.datetime "created_at",                                                                  :null => false
    t.datetime "updated_at",                                                                  :null => false
    t.string   "telefono"
    t.string   "usuario"
    t.decimal  "precioTotal",  :precision => 8, :scale => 2, :default => 0.0
    t.string   "estadoPedido",                               :default => "Pendiente de pago"
    t.string   "localizador",                                :default => "-"
    t.string   "cp"
    t.string   "provincia"
    t.string   "poblacion"
    t.string   "pais",                                       :default => "España"
    t.decimal  "gastosEnvio",  :precision => 8, :scale => 2, :default => 0.0
  end

  create_table "products", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.decimal  "price",            :precision => 8, :scale => 2
    t.string   "extensionImagen"
    t.string   "tipo"
    t.datetime "created_at",                                                      :null => false
    t.datetime "updated_at",                                                      :null => false
    t.string   "extensionImagena"
    t.string   "extensionImagenb"
    t.decimal  "volumen",          :precision => 8, :scale => 4, :default => 0.0
  end

  create_table "users", :force => true do |t|
    t.string   "name"
    t.string   "password_digest"
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
    t.string   "tipo",                   :default => "usuario"
    t.string   "email"
    t.string   "direccion_completa"
    t.string   "telefono"
    t.string   "password_reset_token"
    t.datetime "password_reset_sent_at"
    t.string   "auth_token"
    t.string   "cp"
    t.string   "provincia"
    t.string   "poblacion"
    t.string   "pais",                   :default => "España"
  end

end
