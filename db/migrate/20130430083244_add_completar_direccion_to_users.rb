# encoding: utf-8
class AddCompletarDireccionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :cp, :string
    add_column :users, :provincia, :string
    add_column :users, :poblacion, :string
    add_column :users, :pais, :string,:default => "España"

  end
end
