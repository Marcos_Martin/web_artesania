class CreateGaleriaFotos < ActiveRecord::Migration
  def change
    create_table :galeria_fotos do |t|
      t.string :titulo
      t.string :descripcion
      t.string :formato

      t.timestamps
    end
  end
end
