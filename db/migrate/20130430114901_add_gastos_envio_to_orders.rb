class AddGastosEnvioToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :gastosEnvio, :decimal,:precision => 8, :scale => 2, :default => 0
  end
end
