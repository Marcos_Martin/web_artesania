class AddCompletarUsuarioToUsers < ActiveRecord::Migration
  def change
    add_column :users, :email, :string
    add_column :users, :direccion_completa, :string
    add_column :users, :telefono, :string
  end
end
