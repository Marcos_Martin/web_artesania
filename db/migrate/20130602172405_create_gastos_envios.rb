class CreateGastosEnvios < ActiveRecord::Migration
  def change
    create_table :gastos_envios do |t|
      t.string :tipo,:default =>"rango"
      t.decimal :kilos,:precision => 8, :scale => 2, :default => 0
      t.decimal :precio,:precision => 8, :scale => 2, :default => 0

      t.timestamps
    end
    GastosEnvio.create :tipo => "rango", :kilos => 0, :precio => 0
    GastosEnvio.create :tipo => "rango", :kilos => 0.01, :precio => 4
    GastosEnvio.create :tipo => "rango", :kilos => 5, :precio => 4
    GastosEnvio.create :tipo => "rango", :kilos => 10, :precio => 5
    GastosEnvio.create :tipo => "rango", :kilos => 20, :precio => 7
    GastosEnvio.create :tipo => "rango", :kilos => 30, :precio => 8
    GastosEnvio.create :tipo => "rango", :kilos => 40, :precio => 9
    GastosEnvio.create :tipo => "rango", :kilos => 50, :precio => 10
    GastosEnvio.create :tipo => "rango", :kilos => 60, :precio => 12
    GastosEnvio.create :tipo => "rango", :kilos => 70, :precio => 13
    GastosEnvio.create :tipo => "rango", :kilos => 80, :precio => 14
    GastosEnvio.create :tipo => "rango", :kilos => 90, :precio => 15
    GastosEnvio.create :tipo => "rango", :kilos => 100, :precio => 16
    GastosEnvio.create :tipo => "rango", :kilos => 110, :precio => 17
    GastosEnvio.create :tipo => "rango", :kilos => 120, :precio => 19
    GastosEnvio.create :tipo => "rango", :kilos => 130, :precio => 20
    GastosEnvio.create :tipo => "rango", :kilos => 140, :precio => 22
    GastosEnvio.create :tipo => "rango", :kilos => 150, :precio => 23
    GastosEnvio.create :tipo => "rango", :kilos => 160, :precio => 24
    GastosEnvio.create :tipo => "rango", :kilos => 170, :precio => 25
    GastosEnvio.create :tipo => "rango", :kilos => 180, :precio => 27
    GastosEnvio.create :tipo => "rango", :kilos => 190, :precio => 28
    GastosEnvio.create :tipo => "rango", :kilos => 200, :precio => 29
    GastosEnvio.create :tipo => "rango", :kilos => 225, :precio => 30
    GastosEnvio.create :tipo => "rango", :kilos => 250, :precio => 32
    GastosEnvio.create :tipo => "rango", :kilos => 275, :precio => 33
    GastosEnvio.create :tipo => "rango", :kilos => 300, :precio => 34
    GastosEnvio.create :tipo => "factorConversion", :kilos => -1, :precio => 225
    GastosEnvio.create :tipo => "gastosRembolso", :kilos => -1, :precio => 8





  end

end
