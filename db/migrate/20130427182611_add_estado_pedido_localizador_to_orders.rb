class AddEstadoPedidoLocalizadorToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :estadoPedido, :string, :default => "Pendiente de pago"
    add_column :orders, :localizador, :string, :default => "-"
  end
end
