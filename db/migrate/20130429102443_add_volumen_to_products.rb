class AddVolumenToProducts < ActiveRecord::Migration
  def change
    add_column :products, :volumen,:decimal,:precision => 8, :scale => 4, :default => 0
  end
end
