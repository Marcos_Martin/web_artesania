# encoding: utf-8
class AddCompletarDireccionToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :cp, :string
    add_column :orders, :provincia, :string
    add_column :orders, :poblacion, :string
    add_column :orders, :pais, :string,:default => "España"
  end
end
