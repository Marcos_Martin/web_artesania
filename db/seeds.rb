# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.delete_all
User.create(name: "Admin" ,tipo:"xerif",password: "web_artesamia",
            email: "enlamanchaonline@gmail.com",
            direccion_completa: "-",telefono: "-",cp: "-",provincia:"-",
            poblacion:"-",pais: "-")


GastosEnvio.delete_all
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 0, :precio => 0
GastosEnvio.create :id=>2, :tipo => "rango", :kilos => 0.01, :precio => 4
GastosEnvio.create :id=>3, :tipo => "rango", :kilos => 5, :precio => 4
GastosEnvio.create :id=>4, :tipo => "rango", :kilos => 10, :precio => 5
GastosEnvio.create :id=>5, :tipo => "rango", :kilos => 20, :precio => 7
GastosEnvio.create :id=>6, :tipo => "rango", :kilos => 30, :precio => 8
GastosEnvio.create :id=>7, :tipo => "rango", :kilos => 40, :precio => 9
GastosEnvio.create :id=>8, :tipo => "rango", :kilos => 50, :precio => 10
GastosEnvio.create :id=>9, :tipo => "rango", :kilos => 60, :precio => 12
GastosEnvio.create :id=>10, :tipo => "rango", :kilos => 70, :precio => 13
GastosEnvio.create :id=>11, :tipo => "rango", :kilos => 80, :precio => 14
GastosEnvio.create :id=>12, :tipo => "rango", :kilos => 90, :precio => 15
GastosEnvio.create :id=>13, :tipo => "rango", :kilos => 100, :precio => 16
GastosEnvio.create :id=>14, :tipo => "rango", :kilos => 110, :precio => 17
GastosEnvio.create :id=>15, :tipo => "rango", :kilos => 120, :precio => 19
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 130, :precio => 20
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 140, :precio => 22
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 150, :precio => 23
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 160, :precio => 24
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 170, :precio => 25
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 180, :precio => 27
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 190, :precio => 28
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 200, :precio => 29
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 225, :precio => 30
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 250, :precio => 32
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 275, :precio => 33
GastosEnvio.create :id=>1, :tipo => "rango", :kilos => 300, :precio => 34
GastosEnvio.create :id=>1, :tipo => "factorConversion", :kilos => -1, :precio => 225
GastosEnvio.create :id=>1, :tipo => "gastosRembolso", :kilos => -1, :precio => 8


